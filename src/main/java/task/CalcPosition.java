package task;

import Jama.Matrix;
import convert.ConvertFileRinex;
import enums.Constants;
import enums.ConstantsPath;
import interf.MinSquare;
import nav.convert.Nav;
import nav.model.*;
import obs.convert.RinexObs;
import obs.model.DataObs;
import obs.model.Observation;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static convert.Matr.getDouble;
import static convert.Matr.getVector;
import static convert.Matr.getZeroMatr;

public class CalcPosition {

    public static Matrix getPosition(List<DataObs> obs, List<SatelliteObject> nav, LocalDateTime dt, MinSquare minSquare) throws NullPointerException {
        DataObs rinexObs = RinexObs.searchTimeObs(obs, dt);
        List<Observation> observations = rinexObs != null ? rinexObs.getObservations() : null;
        List<BigDecimal> pr = new ArrayList<>();
        List<Matrix> xSv = new ArrayList<>();
        Matrix xMatr = null;
        if (observations != null) {
            for (Observation observation : observations) {
                //нахожу id спутника по наблюдению
                Integer id = RinexObs.getId(observation);
                SatelliteObject navigation = Nav.getNavTimeID(nav, dt, id);
                //в файле присутствует тип спутника R, но я использую только G, если спутник найден
                if (navigation != null && !observation.getSatelliteTypeId().getType().equals("R")) {
                    //получаю PRN, EPOCH,SV CLK
                    SatellitePRN sPRN = navigation.getSatellitePRN();
                    //из наблюдения получая C1
                    BigDecimal c1 = observation.getC1();
                    Long obsTimeMod = (long) (3600 * (24 + dt.getHour()) + 60 * dt.getMinute() + dt.getSecond());
                    BigDecimal tmp = c1.divide(BigDecimal.valueOf(Constants.C.intValue), 11, RoundingMode.HALF_UP);
                    Double time = obsTimeMod - tmp.doubleValue();
                    ComponentsSatellite cSat = TaskCoordinates.getCoorSat(navigation, null, time);
                    List<CheckCoordinates> list1 = Nav.cInspectionObj(ConvertFileRinex.readFileRinex(ConstantsPath.FILE_SP3.filePath, "CLK:CMB"));
                    ArrayList<Integer> date = new ArrayList<>();
                    date.add(sPRN.getHour());
                    date.add(sPRN.getMinute());
                    date.add(sPRN.getSecond().intValue());
                    ComponentsSatellite componentsSatellite = TaskCoordinates.getCoorSat(navigation, date, null);
                    SatelliteCoordinates sCoordCheck = Nav.getSatCoord(list1, rinexObs.getHour(), rinexObs.getMinute(), rinexObs.getSecond().doubleValue(), id);
                    SatelliteCoordinates sCoord = componentsSatellite.getSatelliteCoordinates();
                    if (sCoordCheck != null)
                        System.out.println(Nav.getDelta(sCoord, sCoordCheck));
                    Double tocTime = time - obsTimeMod;
                    BigDecimal tmp1 = sPRN.getVal("сдвиг часов").add(sPRN.getVal("скорость ухода").multiply(BigDecimal.valueOf(tocTime)));
                    BigDecimal tmp2 = tmp1.add(sPRN.getVal("ускорение ухода").multiply(BigDecimal.valueOf(Math.pow(tocTime, 2))));
                    BigDecimal tmp3 = tmp2.subtract(navigation.getBroadcastOrbitSix().getTgd());
                    BigDecimal dltT = tmp3.add(BigDecimal.valueOf(cSat.getDltTr()));
                    tmp1 = dltT.multiply(BigDecimal.valueOf(Constants.C.intValue));
                    tmp2 = c1.add(tmp1);
                    pr.add(tmp2);
                    BigDecimal deltaT = tmp2.divide(BigDecimal.valueOf(Constants.C.intValue), RoundingMode.HALF_UP);
                    BigDecimal aoR = deltaT.negate().multiply(BigDecimal.valueOf(Constants.EROT.doubleValue));
                    double[][] mor = getZeroMatr(3, 3);
                    mor[0][0] = Math.cos(aoR.doubleValue());
                    mor[1][1] = mor[0][0];
                    mor[0][1] = Math.sin(aoR.doubleValue());
                    mor[1][0] = -mor[0][1];
                    mor[2][2] = 1.0;
                    Matrix matrix = new Matrix(mor);
                    Matrix transMor = matrix.transpose();
                    double[][] coordSat = getVector(cSat.getSatelliteCoordinates());
                    Matrix coordSatVector = new Matrix(coordSat);

                    xSv.add(coordSatVector.times(transMor));
                }
                if (xSv.size() == 4) {
                    break;
                }
            }
            if (xSv.size() < 4)
                return null;
            if (!xSv.isEmpty()) {
                double[][] x = getZeroMatr(4, 1);
                xMatr = new Matrix(x);
                double[][] r = getZeroMatr(4, 1);
                double[][] h = getZeroMatr(4, 4);
                double[][] dPr = getZeroMatr(4, 1);
                for (int i = 0; i < 10; i++) {
                    for (int z = 0; z < 4; z++) {
                        r[z][0] = minSquare.getR(z, xSv, xMatr);
                        xSv.get(z).get(0, 0);
                        h[z][0] = (getDouble(xSv, z, 0) - xMatr.get(0, 0)) / r[z][0];
                        h[z][1] = (getDouble(xSv, z, 1) - xMatr.get(1, 0)) / r[z][0];
                        h[z][2] = (getDouble(xSv, z, 2) - xMatr.get(2, 0)) / r[z][0];
                        h[z][3] = 1;
                        dPr[z][0] = pr.get(z).doubleValue() - r[z][0] - xMatr.get(3, 0);
                    }
                    //d_X = (H'*H)^-1*H'*d_PR;
                    Matrix matrixH = new Matrix(h);
                    Matrix hTrans = matrixH.transpose();
                    Matrix hTransH = hTrans.times(matrixH);
                    Matrix invHtransH = hTransH.inverse();
                    Matrix mulInvTransH = invHtransH.times(hTrans);
                    Matrix dPrMatr = new Matrix(dPr);
                    Matrix dX = mulInvTransH.times(dPrMatr);
                    xMatr = xMatr.minus(dX);
                }
            }
        } else {
            System.out.println("Не нашлось наблюдений спутника");
        }
        return xMatr;
    }
}