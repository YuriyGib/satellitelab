package convert;

import Jama.Matrix;
import nav.model.SatelliteCoordinates;

import java.util.List;

public class Matr {
    public static Double getDouble(List<Matrix> x, int i, int j) {
        return x.get(i).get(0, j);
    }

    public static double[][] getZeroMatr(int n, int m) {
        double[][] d = new double[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                d[i][j] = 0.0;
            }
        }
        return d;
    }

    public static double[][] getXst() {
        double[][] xSt = getZeroMatr(1, 3);
        xSt[0][0] = -2407751;
        xSt[0][1] = -4706536.65;
        xSt[0][2] = 3557571.41;
        return xSt;
    }

    public static double[][] getVector(SatelliteCoordinates c) {
        double[][] v = new double[1][3];
        v[0][0] = c.getX();
        v[0][1] = c.getY();
        v[0][2] = c.getZ();
        return v;
    }
}
