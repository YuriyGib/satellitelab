package nav.model;

import lombok.Getter;
import java.math.BigDecimal;
import java.util.ArrayList;

@Getter
public class BroadcastOrbitSeven {
    private BigDecimal messageTransTime;
    private BigDecimal intervalOrbitApprox;

    public BroadcastOrbitSeven(ArrayList<TypeSatellite> typeSatellites, int i) {
        this.messageTransTime=typeSatellites.get(i).getTypeBigDecimal();
        this.intervalOrbitApprox=typeSatellites.get(i+1).getTypeBigDecimal();
    }

    @Override
    public String toString() {
        return "BroadcastOrbitSeven{" +
                "messageTransTime=" + messageTransTime +
                ", intervalOrbitApprox=" + intervalOrbitApprox +
                '}';
    }
}
