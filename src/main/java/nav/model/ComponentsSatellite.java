package nav.model;

import lombok.Getter;

import java.util.List;

@Getter
public class ComponentsSatellite {
    private String idSatellite;
    private SatelliteCoordinates satelliteCoordinates;
    private SatelliteSpeedCoordinates satelliteSpeedCoordinates;
    private Double dltTr;

    public ComponentsSatellite(SatelliteCoordinates satelliteCoordinates, SatelliteSpeedCoordinates satelliteSpeedCoordinates, Double dltTr) {
        this.satelliteCoordinates = satelliteCoordinates;
        this.satelliteSpeedCoordinates = satelliteSpeedCoordinates;
        this.dltTr = dltTr;
    }

    public ComponentsSatellite(List<String> coord, int i) {
        idSatellite = coord.get(i);
        satelliteCoordinates = new SatelliteCoordinates(Double.valueOf(coord.get(i + 1)), Double.valueOf(coord.get(i + 2)), Double.valueOf(coord.get(i + 3)));

    }

    @Override
    public String toString() {
        return "ComponentsSatellite{" +
                "satelliteCoordinates=" + satelliteCoordinates +
                ", satelliteSpeedCoordinates=" + satelliteSpeedCoordinates +
                '}';
    }
}
