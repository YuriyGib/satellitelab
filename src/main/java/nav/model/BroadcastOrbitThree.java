package nav.model;

import lombok.Getter;
import java.math.BigDecimal;
import java.util.ArrayList;

@Getter
public class BroadcastOrbitThree {
    private BigDecimal ephemerisTime;
    private BigDecimal cic;
    private BigDecimal omega;
    private BigDecimal cis;

    public BroadcastOrbitThree(ArrayList<TypeSatellite> typeSatellites,int i) {
        this.ephemerisTime = typeSatellites.get(i).getTypeBigDecimal();
        this.cic = typeSatellites.get(i+1).getTypeBigDecimal();
        this.omega = typeSatellites.get(i+2).getTypeBigDecimal();
        this.cis = typeSatellites.get(i+3).getTypeBigDecimal();
    }

    @Override
    public String toString() {
        return "BroadcastOrbitThree{" +
                "ephemerisTime=" + ephemerisTime +
                ", cic=" + cic +
                ", omega=" + omega +
                ", cis=" + cis +
                '}';
    }
}
