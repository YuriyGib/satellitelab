package nav.model;

import lombok.Getter;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Getter
public class DeltaCoordinates {
    private BigDecimal dx;
    private BigDecimal dy;
    private BigDecimal dz;

    public DeltaCoordinates(BigDecimal dx, BigDecimal dy, BigDecimal dz) {
        this.dx = dx;
        this.dy = dy;
        this.dz = dz;
    }

    @Override
    public String toString() {
        return "Отклонение{" +
                "dx=" + dx.setScale(6, RoundingMode.HALF_UP).toString() +
                ", dy=" + dy.setScale(6,RoundingMode.HALF_UP).toString()+
                ", dz=" + dz.setScale(6,RoundingMode.HALF_UP).toString() +
                '}';
    }
}
