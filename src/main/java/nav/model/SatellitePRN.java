package nav.model;

import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Getter
public class SatellitePRN {
    private Integer id;
    private Integer year;
    private Integer month;
    private Integer day;
    private Integer hour;
    private Integer minute;
    private BigDecimal second;
    private LocalDateTime time;
    private BigDecimal satelliteClockShift;
    private BigDecimal speedOfCareHours;
    private BigDecimal accelerationCareHours;

    public BigDecimal getVal(String s) {
        try {
            switch (s) {
                case "сдвиг часов":
                    return satelliteClockShift;
                case "секунда":
                    return second;
                case "скорость ухода":
                    return speedOfCareHours;
                case "ускорение ухода":
                    return accelerationCareHours;
                default:
                    throw new NumberFormatException();
            }
        } catch (NumberFormatException ex) {
            System.out.println("Неверный формат");
            return null;
        }
    }

    public SatellitePRN(ArrayList<TypeSatellite> typeSatellites, int i) {
        this.id = typeSatellites.get(i).getTypeInteger();
        this.year = 2000 + typeSatellites.get(i + 1).getTypeInteger();
        this.month = typeSatellites.get(i + 2).getTypeInteger();
        this.day = typeSatellites.get(i + 3).getTypeInteger();
        this.hour = typeSatellites.get(i + 4).getTypeInteger();
        this.minute = typeSatellites.get(i + 5).getTypeInteger();
        this.second = typeSatellites.get(i + 6).getTypeBigDecimal();
        this.satelliteClockShift = typeSatellites.get(i + 7).getTypeBigDecimal();
        this.speedOfCareHours = typeSatellites.get(i + 8).getTypeBigDecimal();
        this.accelerationCareHours = typeSatellites.get(i + 9).getTypeBigDecimal();
        this.time = LocalDateTime.of(this.year, month, day, hour, minute, second.intValue());
    }

    @Override
    public String toString() {
        return "SatellitePRN{" +
                "numberSatellite=" + id +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", hour=" + hour +
                ", minute=" + minute +
                ", second=" + second +
                ", satelliteClockShift=" + satelliteClockShift +
                ", speedOfCareHours=" + speedOfCareHours +
                ", accelerationCareHours=" + accelerationCareHours +
                '}';
    }
}
