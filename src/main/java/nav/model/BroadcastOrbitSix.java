package nav.model;

import lombok.Getter;
import java.math.BigDecimal;
import java.util.ArrayList;

@Getter
public class BroadcastOrbitSix {
    private BigDecimal satellitePosAccuracy;
    private BigDecimal soundness;
    private BigDecimal tgd;
    private BigDecimal iodc;

    public BroadcastOrbitSix(ArrayList<TypeSatellite> typeSatellites, int i) {
        this.satellitePosAccuracy = typeSatellites.get(i).getTypeBigDecimal();
        this.soundness = typeSatellites.get(i + 1).getTypeBigDecimal();
        this.tgd = typeSatellites.get(i + 2).getTypeBigDecimal();
        this.iodc = typeSatellites.get(i + 3).getTypeBigDecimal();
    }

    @Override
    public String toString() {
        return "BroadcastOrbitSix{" +
                "satellitePosAccuracy=" + satellitePosAccuracy +
                ", soundness=" + soundness +
                ", tgd=" + tgd +
                ", iodc=" + iodc +
                '}';
    }
}
