package obs.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class SatelliteTypeId {
    private Integer id;
    private String type;

    public SatelliteTypeId(Integer id, String type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public String toString() {
        return "\n" + type + id + "\n";
    }
}
