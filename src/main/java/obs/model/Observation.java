package obs.model;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class Observation {
    private BigDecimal l1;
    private Integer signalStrengthOne;
    private BigDecimal l2;
    private Integer signalStrengthTwo;
    private BigDecimal c1;
    private BigDecimal p2;
    private BigDecimal p1;
    private BigDecimal s1;
    private BigDecimal s2;
    private SatelliteTypeId satelliteTypeId;

    @Override
    public String toString() {
        return "Observation{" + "\n" +
                ", Тип спутника и id = " + satelliteTypeId + "\n" +
                "l1 = " + l1 + "\n" +
                ", Сила сигнала " + signalStrengthOne + "\n" +
                ", l2 = " + l2 + "\n" +
                ", Сила сигнала " + signalStrengthTwo + "\n" +
                ", c1 = " + c1 + "\n" +
                ", p2 = " + p2 + "\n" +
                ", p1 = " + p1 + "\n" +
                ", s1 = " + s1 + "\n" +
                ", s2 = " + s2 + "\n" +
                '}';
    }

    public Observation(BigDecimal l1, Integer signalStrengthOne, BigDecimal l2, Integer signalStrengthTwo, BigDecimal c1, BigDecimal p2, BigDecimal p1, BigDecimal s1, BigDecimal s2, SatelliteTypeId satelliteTypeId) {
        this.l1 = l1;
        this.signalStrengthOne = signalStrengthOne;
        this.l2 = l2;
        this.signalStrengthTwo = signalStrengthTwo;
        this.c1 = c1;
        this.p2 = p2;
        this.p1 = p1;
        this.s1 = s1;
        this.s2 = s2;
        this.satelliteTypeId = satelliteTypeId;
    }
}
