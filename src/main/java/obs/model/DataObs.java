package obs.model;

import lombok.Getter;

import java.math.BigDecimal;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.GregorianCalendar;
import java.util.List;

@Getter
public class DataObs {
    private Integer year;
    private Integer month;
    private Integer day;
    private Integer hour;
    private Integer minute;
    private BigDecimal second;
    private LocalDateTime time;
    private Integer era;
    private Integer countSat;
    private List<SatelliteTypeId> satellite;
    //Данные наблюдения
    private List<Observation> observations;

    public DataObs(Integer year, Integer month, Integer day, Integer hour, Integer minute, BigDecimal second, Integer era, Integer countSat, List<SatelliteTypeId> satellite, List<Observation> observations) {
        this.year = 2000 + year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.time = LocalDateTime.of(this.year, month, day, hour, minute, second.intValue());
        this.era = era;
        this.countSat = countSat;
        this.satellite = satellite;
        this.observations = observations;
    }

    @Override
    public String toString() {
        return "Данные наблюдения {" + "\n" +
                "Год = " + year + "\n" +
                ", Месяц = " + month + "\n" +
                ", День = " + day + "\n" +
                ", Час = " + hour + "\n" +
                ", Минута = " + minute + "\n" +
                ", second = " + second + "\n" +
                ", Эпоха = " + era + "\n" +
                ", Количество Спутников = " + countSat + "\n" +
                ", Спутники = " + satellite + "\n" +
                ", Наблюдение = " + observations + "\n" +
                '}';
    }
}
