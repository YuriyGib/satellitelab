package obs.convert;

import convert.ConvertFileRinex;
import obs.model.DataObs;
import obs.model.Observation;
import obs.model.SatelliteTypeId;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;


public class RinexObs {


    private static Integer stringToInt(String s) {
        return Integer.valueOf(s);
    }

    private static BigDecimal stringToBigDec(String s) {
        try {

            if (s != null && !s.isEmpty()) {
                return BigDecimal.valueOf(stringToDouble(s));
            } else
                return null;
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    private static List<String> removeLastChar(String s) {
        List<String> list = new ArrayList<>();
        String[] nameSatArr = s.split("^\\d\\d");
        if (nameSatArr.length == 1)
            nameSatArr = s.split("^\\d");
        String name = nameSatArr[1];

        int count = s.length() - name.length();
        list.add(s.substring(0, count));
        list.add(name);
        return list;
    }

    @NotNull
    private static Double stringToDouble(String s) {
        return Double.valueOf(s);
    }

    private static List<SatelliteTypeId> convertSat(String s) {
        try {

            List<SatelliteTypeId> satelliteTypeIds = new ArrayList<>();
            s = ConvertFileRinex.replaceG(s, "G", " G");
            s = ConvertFileRinex.replaceG(s, "E", " E");
            s = ConvertFileRinex.replaceG(s, "R", " R");
            s = ConvertFileRinex.replaceG(s, "S", " S");
            s = ConvertFileRinex.replaceG(s, "M", " M");
            String[] sArray = s.split(" ");
            for (String sat : sArray) {
                if (!sat.isEmpty()) {
                    String type = String.valueOf(sat.charAt(0));
                    Integer id = stringToInt(sat.substring(1));
                    satelliteTypeIds.add(new SatelliteTypeId(id, type));
                }
            }
            return satelliteTypeIds;
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    private static String convertToString(String str, int start, int end) {
        return str.length() > end - 1 ? str.substring(start, end).trim() : null;
    }

    private static List<String> arrStr(String[] arrS) {
        List<String> list = new ArrayList<>();
        for (String s : arrS) {
            if (!s.isEmpty()) {
                list.add(s);
            }
        }
        return list;
    }

    public static Integer getId(Observation obs) {
        return obs.getSatelliteTypeId().getId();
    }

    public static String getType(Observation obs) {
        return obs.getSatelliteTypeId().getType();
    }

    public static DataObs searchTimeObs(List<DataObs> dataObs, LocalDateTime dt) {
        for (DataObs obs : dataObs) {
            if (obs.getTime().isEqual(dt)) {
                return obs;
            }
        }
        return null;
    }

    @Nullable
    public static List<DataObs> convertROF(List<String> list) {
        List<DataObs> dataObs = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            List<String> str1 = arrStr(list.get(i).trim().split(" "));
            Integer year = stringToInt(str1.get(0).trim());
            Integer month = stringToInt(str1.get(1).trim());
            Integer day = stringToInt(str1.get(2).trim());
            Integer hour = stringToInt(str1.get(3).trim());
            Integer minute = stringToInt(str1.get(4).trim());
            BigDecimal second = stringToBigDec(str1.get(5).trim());
            Integer era = stringToInt(str1.get(6).trim());
            List<String> satData = removeLastChar(str1.get(7).trim());
            Integer countSat = stringToInt(satData.get(0));
            String satTwoLine = "";
            //если больше 12 спутников
            if (countSat > 12) {
                satTwoLine = list.get(++i).trim();
            }
            List<SatelliteTypeId> satellite = convertSat(satData.get(1).concat(satTwoLine));
            List<Observation> observations = new ArrayList<>();
            for (int j = 0; j < countSat; j++) {
                //считываем вторую(третью) строку
                String str2 = list.get(++i);
                String[] l1Str = convertToString(str2, 0, 16).split(" ");
                String[] l2Str = convertToString(str2, 17, 32).split(" ");

                String c1Str = convertToString(str2, 33, 46);
                String p2Str = convertToString(str2, 49, 62);
                String p1Str = convertToString(str2, 65, 78);
                BigDecimal l1 = null;
                if (l1Str.length > 0 && !l1Str[0].equals(""))
                    l1 = stringToBigDec(l1Str[0]);
                Integer sS1 = null;
                if (l1Str.length > 1 && !l1Str[1].equals(""))
                    sS1 = stringToInt(l1Str[1]);
                BigDecimal l2 = null;
                if (l2Str.length > 0 && !l2Str[0].equals(""))
                    l2 = stringToBigDec(l2Str[0]);
                Integer sS2 = null;
                if (l2Str.length > 1 && !l2Str[1].equals(""))
                    sS2 = stringToInt(l2Str[1]);
                BigDecimal c1 = stringToBigDec(c1Str);
                BigDecimal p2 = stringToBigDec(p2Str);
                BigDecimal p1 = stringToBigDec(p1Str);
                //считываем 3 строку
                String str3 = list.get(++i);
                String s1Str = convertToString(str3, 0, 14);
                String s2Str = convertToString(str3, 15, 30);
                BigDecimal s1 = stringToBigDec(s1Str);
                BigDecimal s2 = stringToBigDec(s2Str);
                observations.add(new Observation(l1, sS1, l2, sS2, c1, p2, p1, s1, s2, satellite.get(j)));
            }
            dataObs.add(new DataObs(year, month, day, hour, minute, second, era, countSat, satellite, observations));
        }
        return dataObs;
    }
}
