package enums;

public enum ConstantsPath {
    //FILE_NAV("src/main/java/data/p0162950.17n"),
    FILE_NAV("src/main/java/data/7odm0550.16n"),
    FILE_OBS("src/main/java/data/7odm0550.16o"),
    //FILE_SP3("src/main/java/data/igu19721_00.sp3");
    FILE_SP3("src/main/java/data/igu18854_00.sp3");
    public String filePath;

    ConstantsPath(String s) {
        filePath = s;
    }
}