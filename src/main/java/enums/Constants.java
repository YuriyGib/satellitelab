package enums;

public enum Constants {
    EROT(7.2921151467e-5),
    M(3.986005E+14),
    EPS(0.0001),
    EARTH(7.29211511467E-5),
    C(299792458);

    public int intValue;
    public double doubleValue;

    Constants(int i) {
        intValue = i;
    }

    Constants(double v) {
        doubleValue = v;
    }
}
